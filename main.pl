#/usr/bin/perl -w

use strict;

use Srt;
use Dictionary;

my $srt = new Srt();
my @ignore = ("the",
              "you",
              "i",
              "to",
              "not",
              "do",
              "has",
              "it",
              "and",
              "of",
              "in",
              "for",
              "have",
              "but",
              "is",
              "soda",
              "cola",
              "your",
              "we",
              "school",
              "a",
              "that",
              "are",
              "so",
              "about",
              "this",
              "on",
              "an",
              "am",
              "oh",
              "was",
              "ultra",
);

my $dict = Dictionary->new(
    ignore  => \@ignore,
    mode    => Dictionary::DICTIONARY_WORDS,
);

$srt->open($ARGV[0]) or print("error " + $srt->{error});

while(my $line = $srt->next_line())
{
    $dict->add_words($line);
}

print map {"$dict->{words}->{$_} \t \"$_\"\n"} keys %{$dict->{words}};




