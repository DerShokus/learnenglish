package Dictionary;

use strict;
use warnings;

###
# Parameters of words for add to dictionary
#
use constant {
    DICTIONARY_WORDS        => 0x1,     # 0001
    DICTIONARY_NUMBERS      => 0x2,     # 0010
    DICTIONARY_KEEP_SYMBOLS => 0x4,     # 0100
    DICTIONARY_HONORIFICS   => 0x8,     # 1000
    DICTIONARY_ALL          => 0x9,     # 1111
};


our @REDUCTIONS = (
    # reduction     complete form
    [ "don't",      "do not"     ],
    [ "doesn't",    "does not"   ],
    [ "doesn't",    "does not"   ],
    [ "didn't",     "did not"    ],
    [ "won't",      "will not"   ],
    [ "can't",      "can not"    ],
    [ "couldn't",   "could not"  ],
    [ "mustn't",    "must not"   ],
    [ "oughtn't",   "ought not"  ],
    [ "aren't",     "are not"    ],
    [ "isn't",      "is not"     ],
    [ "wasn't",     "was not"    ],
    [ "weren't",    "were not"   ],
    [ "haven’t",    "have not"   ],
    [ "hasn’t",     "has not"    ],
    [ "hadn’t",     "had not"    ],
    [ "wouldn't",   "would not"  ],
    [ "shouldn't",  "should not" ],

);

our @REDUCTIONS_REGEXP = (
    # reduction     complete form
    [ "d",     "had"        ],
    [ "d",     "would"      ],
    [ "ve",    "have"       ],
    [ "s",     "has"        ],
    [ "ll",    "will"       ],
    [ "m",     "am"         ],
    [ "re",    "are"        ],
    [ "s",     "is"         ],
);


###############################################################################
# The object constructor                                                      #
###############################################################################
sub new {
    my ($class, %hash) = @_;
    my $self = {};      
    # initialize members
    $self->{mode}   = $hash{mode};;
    $self->{words}  = {};               # words for my dictionary
    $self->{ignore} = $hash{ignore};    # ignore list (don't add to dictionary)
    # ok, let's go
    bless $self, $class;
    return $self;
}


###############################################################################
# Add line of words and returns count of added words                          #
###############################################################################
sub add_words {
    my $self = shift;
    my $words = shift;
    my $count = 0;
    my $long_word = '';

    chomp($words);
    my @words = split(' ',$words);
    foreach my $word (@words) {
        if ($word =~ /^[Mm]([Rr]|[Ss]$)/) {
            $long_word = $word;
            next;
        }
        if (length $long_word) {
            $long_word .= ' ' . $word;
            $count+=$self->add_word($long_word);
            $long_word = '';
        }
        else {
            $count+=$self->add_word($word);
        }
    }
    return $count;
}


###############################################################################
# Add a word                                                                  #
###############################################################################
sub add_word {
    my $self = shift;
    my $word = shift;
    # delete stupid symbols
    chomp($word);
    $word = lc $word;
    $word =~ s/^[,:\.\!"\?\-\']+//g;                     # delte , : . ! ? -
    $word =~ s/[,:\.\!"\?\-\']+$//g;                     # at start and end
    # check block
    return 0 unless (defined $word or length $word);     # is word empty?
    return 0 if (grep(/^$word$/, @{$self->{ignore}}));   # is it ingore word?
    return 0 if (!$self->check($word));                 
    # is it reduction?
    return $self->add_words($word) if ($self->expand(\$word));
    # add word and count it
    $self->{words}->{$word}++;
    # all right
    return 1;
}


###############################################################################
# Add a word                                                                  #
###############################################################################
sub check {
    my ($self, $word) = @_;
    # is it a string?
    return 0 if (ref $word);
    return 0 if ($word =~ /^\s*$/);                 # is it just a spaces?

    unless ($self->{mode} & DICTIONARY_NUMBERS) {
        return 0 if ($word =~ /^\d+$/);             # is it number?
        return 0 if ($word =~ /^(\d+,)+\d$/);       # is it some like 1,200
        return 0 if ($word =~ /^(\d+\.)+\d$/);      # is it like C number
        return 0 if ($word =~ /^[0-2]?\d\:\d{2}$/); # is it time?
    }

    unless ($self->{mode} & DICTIONARY_HONORIFICS) {
        return 0 if ($word =~ /^[Mm]([Ss]|[Rr])\.\s\w+$/);
    }
    
    return 1;
}


###############################################################################
# Expand a reduction                                                          #
###############################################################################
sub expand {
    my ($self, $word) = @_;
    # only ref
    return 0 if (ref($word) ne 'SCALAR');
    my $wref = $word;
    $word = $$word;
    # if the word contains ' it's may be a reduce
    if (index($word, "'") == -1) {
        return 0;
    }
    # let's check simple words
    foreach my $couple (@REDUCTIONS) {
        if ($word eq $couple->[0]) {
            $$wref = $couple->[1];
            return 1;
        }
    }
    # let's check ending reduce
    my @parts = split("'", $word);
    return if (scalar(@parts) != 2);
    foreach my $couple (@REDUCTIONS_REGEXP) {
        if ($parts[1] eq $couple->[0]) {
            $$wref = "$parts[0] $couple->[1]";
            return 1;
        }
    }
    
    return 0;
}

# ending true
1;
