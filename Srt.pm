package Srt;

use strict;
use warnings;

###
# Constants for parsing
###
use constant {
    SUBTITLE_ID   => 0,  # id likes 001, 1 or some singl number on a line
    SUBTITLE_TIME => 1,  # time start --> end (end and start like 01:01:01,001)
    SUBTITLE_TEXT => 2,  # text just text
};

###
# The object constructor
###

sub new {
    my $class = shift;
    my $self = {};
    # initialize members
    $self->{mode} = SUBTITLE_ID;
    # ok, lets go
    bless $self, $class;
    return $self;
}

###
# Read the file
###
sub open {
    my $self = shift;
    my $path = shift;
    my $file = undef;

    unless(open($file, "<", $path))
    {
        $self->{error} = "cannot open < \"$path\" ($!)";
        return 0;
    }

    $self->{path} = $path;
    $self->{file} = $file;
    
    return 1;
}

###
# Get the next line
###
sub next_line
{
    my $self = shift; 
    my $text = '';      # text for return
    my $ok   = 0;
    while(readline($self->{file}))
    {
        chomp;
        s/\r//;
        next unless(length);
        if($self->{mode}==SUBTITLE_ID || $self->{mode}==SUBTITLE_TEXT)
        {
            # is it id of a subtitle?
            if(/^\d+$/)
            {
                if(defined $self->{last_id} && $self->{last_id}>=$_)
                {
                    return 0;
                }
                $self->{mode} = SUBTITLE_TIME;
                $self->{last_id} = $_;
                if($ok) { return $text; }
                else    { $ok=1;  next; }
            }
        }
        # is it time?
        if($self->{mode}==SUBTITLE_TIME && /^\d{2}:\d{2}:\d{2},\d{3} --> \d{2}:\d{2}:\d{2},\d{3}$/)
        {
            $self->{mode} = SUBTITLE_TEXT;
            next;
        }
        # this is text of subtitle
        if($self->{mode}==SUBTITLE_TEXT)
        {
            $text .= $_ . ' ';
        }
    }
    return $text;
}

1;
